package BasicSeleniumTests;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class LaunchingChromeBrowser {
	
	// some new comment added
	public static void main(String[] args) throws InterruptedException {
		
		String userDir = System.getProperty("user.dir");
		System.setProperty("webdriver.chrome.driver", userDir+"/exeFiles/customNameForChromeDriver.exe");
		ChromeDriver chromeDriver = new ChromeDriver();
		//ChromeDriver chromeDriver1 = new ChromeDriver();
		
		
		// To load URL
		chromeDriver.get("http://automationpractice.com/index.php");
		System.out.println(chromeDriver.getTitle());
		System.out.println(chromeDriver.getCurrentUrl());
		
		//chromeDriver1.get("http://facebook.com/");
		
//		chromeDriver.get("http://google.com/");
//		System.out.println(chromeDriver.getTitle());
//		System.out.println(chromeDriver.getCurrentUrl());
//		
//		chromeDriver.get("http://facebook.com/");
//		System.out.println(chromeDriver.getTitle());
//		System.out.println(chromeDriver.getCurrentUrl());
//		
//		
//		chromeDriver.navigate().back();
//		chromeDriver.navigate().back();
//		chromeDriver.navigate().forward();
//		chromeDriver.navigate().forward();
		
		
		//chromeDriver.manage().window().maximize();
		//chromeDriver.manage().window().fullscreen();
		
		//Thread.sleep(5000);
		
		
//		String actualTitle = chromeDriver.getTitle();
//		String expectedTitle = "My Store";
//		
//		boolean isTitleExpected = actualTitle.equals(expectedTitle);
//		System.out.println(isTitleExpected);
//		
//		
//		
//		String windowHanlde = chromeDriver.getWindowHandle();
//		System.out.println(windowHanlde);
//		
//		
//		String pageSource = chromeDriver.getPageSource();
//		//System.out.println(pageSource);
//		
//		
//		//chromeDriver.executeScript(script, args)
//		
//		String status = (String) chromeDriver.executeScript("return document.readyState");
//		System.out.println(status);
		
		//chromeDriver.close();
		
		//chromeDriver.quit();
		
		
		//chromeDriver.
		
		
		String browserName = chromeDriver.getCapabilities().getBrowserName();
		System.out.println(browserName);
		
		
		if(browserName.equalsIgnoreCase("chrome"))
			System.out.println("Application is only supporte din chrome.");
		
		String version = chromeDriver.getCapabilities().getVersion();
		System.out.println(version);
		
		
		//"selenium".split("e")
		// 72.34.54.65
		
		int versionNumber = Integer.parseInt(version.split("\\.")[0]);
		
		System.out.println(versionNumber);
		
		if(versionNumber >= 60)
			System.out.println("Application support sbrowser version greater than 60.");
		
		
		
//		chromeDriver.navigate().to("http://automationpractice.com/index.php");
//		System.out.println(chromeDriver.getTitle());
//		System.out.println(chromeDriver.getCurrentUrl());
		
		// added a line of code
		chromeDriver.close();
		
		
	}

}
